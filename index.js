'use strict'

const express = require('express');
let server = express();
let port = process.env.PORT || 3000;

server.use((req, res, next)=> {
    res.send({data: 3});
});

console.log('server listening on port ' + port);
server.listen(port);

module.exports = server;
