const request = require('supertest');
const api = require('../index');

describe('When testing the API', () => {
    it('should return 1', ()=> {
        return request(api)
        .get('/')
        .expect(200, {
            data: 3
        })
        .expect('Content-Type', /json/);
    });
});
